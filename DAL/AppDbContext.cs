﻿using System;
using Domain;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public class AppDbContext: IdentityDbContext<User,IdentityRole<int>,int>
    {
        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<CategoryProducts> CategoryProducts { get; set; }
        public DbSet<UserOrders> UserOrders { get; set; }
        public DbSet<OrderProducts> OrderProducts { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<User> User { get; set; }
        
        public AppDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            
        }

    }
    
}
// dotnet ef migrations add InitialDbCreation --project DAL --startup-project WebApp

// dotnet ef migrations remove --project DAL --startup-project WebApp
