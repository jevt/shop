﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Services;

namespace WebShop.Controllers
{
    public class CartController : Controller
    {
       private static ICartService<Product> _cart = new CartService();
     
        private AppDbContext _context;

        public CartController(AppDbContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            
                ViewBag.cartItems = _cart.GetAll();
              Console.WriteLine(_cart.GetAll().Count);
              return View();
        }

        
        public IActionResult Add(int? id)
        {
            Product p = _context.Products.First(product => product.ProductId == id);
          
            _cart.Add(p);
            Console.WriteLine(_cart.GetAll().Count);

            return RedirectToAction(nameof(Index));
        }

        public IActionResult Checkout()
        {
            throw new System.NotImplementedException();
        }

        
        
        public IActionResult Delete(int  id)
        {
            _cart.Del(id);
            return RedirectToAction(nameof(Index));
        }
    }
}