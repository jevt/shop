﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Domain;

namespace Services
{
    public class CartService : ICartService<Product>
    {
        private IDictionary<Product, int> _cart = new Dictionary<Product, int>();
        
        public void Add(Product product)
        {
            if (_cart.ContainsKey(product))
            {
                _cart[product] += 1;
            }
            else
            {
                _cart.Add(product,1);
            }
        }

        public void Del(int id)
        {
              var product = _cart.Keys.FirstOrDefault(product1 => product1.ProductId == product1.ProductId);

            _cart.Remove(product);
            
         }

        

        public IDictionary<Product, int> GetAll()
        {
            return _cart;
        }
    }
}