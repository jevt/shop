﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Services
{
    public interface ICartService<T>
    where T: class
    {
        void Add(T t);

        void Del(int id);

        IDictionary<T, int> GetAll();
    }
}