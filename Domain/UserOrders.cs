﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    public class UserOrders
    {
        public int UserOrdersId { get; set; }


        public int OrderId { get; set; }
        
        public Order? Order { get; set; }

        [ForeignKey("UserId")]
        public int Id { get; set; }
        
        public User? User { get; set; }
        
    }
}




// dotnet aspnet-codegenerator controller -name UsersController          -actions -m User          -dc AppDbContext -outDir Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f
 // dotnet aspnet-codegenerator controller -name CategoriesController   -actions -m Category   -dc AppDbContext -outDir Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f
// dotnet aspnet-codegenerator controller -name ProductsController   -actions -m Product   -dc AppDbContext -outDir Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f
// dotnet aspnet-codegenerator controller -name CategoryProductsController   -actions -m CategoryProducts   -dc AppDbContext -outDir Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f

// dotnet aspnet-codegenerator controller -name UserController  -actions -m User   -dc AppDbContext -outDir Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f
//dotnet aspnet-codegenerator controller -name UserOrdersController  -actions -m UserOrders   -dc AppDbContext -outDir Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f
//dotnet aspnet-codegenerator controller -name OrdersController  -actions -m Order   -dc AppDbContext -outDir Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f

//dotnet aspnet-codegenerator controller -name OrderProductsController  -actions -m OrderProducts   -dc AppDbContext -outDir Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f