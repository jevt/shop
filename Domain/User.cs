﻿using System.Collections.Generic;
 using Microsoft.AspNetCore.Identity;

namespace Domain
{
    public class User : IdentityUser<int>
    {
        // public int UserId { get; set; }
        // public override int Id { get; set; }
        public override string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public override string Email { get; set; }
        public ICollection<UserOrders>? Orders { get; set; }
       
    }
}

