﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    public class Order
    {
        public int OrderId { get; set; }
        
        public DateTime CreatedAt { get; } = DateTime.Now;

        [ForeignKey("UserId")]
        public int UserId { get; set; }
        
        public User? User { get; set; }

        public decimal TotalPrice { get; set; }

        public ICollection<OrderProducts>? ProductsOrdered { get; set; }
    }
}